package org.elu.learn.spring.geocoderclientjava.services;

import org.elu.learn.spring.geocoderclientjava.dao.SiteRepository;
import org.elu.learn.spring.geocoderclientjava.entities.Site;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

@Service
@Transactional
public class SiteService {
    private final SiteRepository repository;
    private final GeocoderService geocoderService;

    public SiteService(SiteRepository repository,
                       GeocoderService geocoderService) {
        this.repository = repository;
        this.geocoderService = geocoderService;
    }

    private Site fillInLatLng(final Site site) {
        return geocoderService.updateSiteLatLng(site);
    }

    public void initialiseDatabase() {
        repository.saveAll(
                List.of(
                        fillInLatLng(new Site("Boston, MA")),
                        fillInLatLng(new Site("Framingham, MA")),
                        fillInLatLng(new Site("Waltham, MA"))
                )
        ).forEach(System.out::println);
    }

    public List<Site> getAllSites() {
        return repository.findAll();
    }

    public Site saveSite(final String address) {
        return repository.save(fillInLatLng(new Site(address)));
    }

    public Optional<Site> findSiteById(final Integer id) {
        return repository.findById(id);
    }
}
