package org.elu.learn.spring.geocoderclientjava;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class GeocoderClientJavaApplication {

    public static void main(String[] args) {
        SpringApplication.run(GeocoderClientJavaApplication.class, args);
    }

}
