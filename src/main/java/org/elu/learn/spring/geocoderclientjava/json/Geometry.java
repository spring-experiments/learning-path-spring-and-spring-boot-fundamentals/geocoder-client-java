package org.elu.learn.spring.geocoderclientjava.json;

public record Geometry(Location location) {
}
