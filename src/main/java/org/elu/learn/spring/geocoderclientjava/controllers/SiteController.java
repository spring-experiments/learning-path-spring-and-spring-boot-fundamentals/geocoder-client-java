package org.elu.learn.spring.geocoderclientjava.controllers;

import org.elu.learn.spring.geocoderclientjava.entities.Site;
import org.elu.learn.spring.geocoderclientjava.services.SiteService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import java.util.List;

@RestController
@RequestMapping("/sites")
public class SiteController {
    private final SiteService service;

    public SiteController(SiteService service) {
        this.service = service;
    }

    @GetMapping
    public List<Site> findAll() {
        return service.getAllSites();
    }

    @GetMapping("{id}")
    public ResponseEntity<Site> findById(final @PathVariable Integer id) {
        return ResponseEntity.of(service.findSiteById(id));
    }

    @PostMapping
    public ResponseEntity<Site> saveSite(@RequestParam String address) {
        final var site = service.saveSite(address);
        final var uri = ServletUriComponentsBuilder.fromCurrentRequestUri()
                .path("/{id}")
                .buildAndExpand(site.getId())
                .toUri();
        return ResponseEntity.created(uri).body(site);
    }
}
