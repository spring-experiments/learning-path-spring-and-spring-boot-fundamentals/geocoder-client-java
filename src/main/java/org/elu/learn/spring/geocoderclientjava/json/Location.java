package org.elu.learn.spring.geocoderclientjava.json;

public record Location(double lat, double lng) {
}
