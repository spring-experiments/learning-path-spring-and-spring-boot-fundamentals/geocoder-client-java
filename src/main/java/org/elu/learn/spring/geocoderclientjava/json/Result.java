package org.elu.learn.spring.geocoderclientjava.json;

import com.fasterxml.jackson.annotation.JsonProperty;

public record Result(@JsonProperty("formatted_address") String formattedAddress, Geometry geometry) {
}
