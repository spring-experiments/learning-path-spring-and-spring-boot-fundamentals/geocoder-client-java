package org.elu.learn.spring.geocoderclientjava.dao;

import org.elu.learn.spring.geocoderclientjava.entities.Site;
import org.springframework.data.jpa.repository.JpaRepository;

public interface SiteRepository extends JpaRepository<Site, Integer> {
}
